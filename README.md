# RateMyDining
- RateMyDining is a website where students can rate their campus restaurants. 
- Team members: Anthony Torns, Aaron Giroux, Christian Casper, and Eric Guerra.
- We are making this website so that students can more easily find good restaraunts and make eating a more social experience.

## Table of Contents

- [General Info](#general-information)
- [Technologies Used](#technologies-used)
- [Features](#features)
- [Setup](#setup)
- [Usage](#usage)
- [Contributions](#contributions)
- [Project Status](#project-status)
- [Room for Improvement](#room-for-improvement)
- [Acknowledgements](#acknowledgements)



## General Information
- The main features of this website are the restaraunt rating system and having this data tied to an account.
- Users can then socialize by comparing ratings, and see where other users are eating.
- This website will make finding good restaraunts interactive, fun, and easy. 
- We believe that a website like this will be useful to ourselves as well as anyone else who is hungry!
- Our project logo https://ibb.co/grCJm9t



## Technologies Used

### Front End
- HTML
- CSS
- JavaScript
- React(Javascript Framework)
### Back End
#### Server
- Java
- Spring(Serverside Java Framework)
#### Database
- SQL
- MySQL(database)

## Features

List the ready features here:

- Restaraunt Rating system - this feature allows students to leave reviews on campus restaurants and came from Casper's user story
- Personal accounts - that track user data, whether that be ratings, diet, or other forms of data - this feature came from Aaron's user story
- Social feature - that allow users to interact and comment on each other's data - this feature came from Anthony's user story
- Verification feature - allows users to register for the website and become verified by verifiying a student email from their campus - this came from Erick's user story
 



## Setup

What are the project requirements/dependencies? Where are they listed? A requirements.txt or a Pipfile.lock file perhaps? Where is it located?

Proceed to describe how to install / setup one's local environment / get started with the project.

## Usage

- All a user would need to do is make an account and start using it. As the user begins browsing restaraunts, adding favorites, and rating locations, the website will begin to keep this data and build a review record. 



## Project Status

- Project is in development
- Sprint 1 is complete
- Sprint 2 is complete
- Sprint 3 is complete

# Contributions
## Sprint 1:
#### Anthony

Researched Technologies to be used

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/d9e34df3726cc4d63046c48880b229efb9e8028d

Research My SQL Requirement

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/b2a7aff40852908dc91f5a74f4c51ce6cd03ab5a

Added database Diagram using draw database

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/255f749f5334ba8b4651488c27f24640314bd653
 
Create Database with specific tables and relationships

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/cf27eabff8225833b7501829224b8cb181418a4d
 
Create AWS Database 

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/1923c2c49841b548131f50403334bd1c085c6488

Add tables to aws 

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/1923c2c49841b548131f50403334bd1c085c6488

#### Christian
Developed the home page using html and CSS

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/65b728262d0708c822b2cf75bc5e4c2eb21bcb78
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/517581b3ad9abe8f95c29ace4bcbc641c4108114
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/070daf5eabf1385977fcea489e0e7efa7852360b

Designed implemented the frontend for the login page

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/fd229fdda3b871220681d6a670d321b1b56ecfaf

Researched front-end specifics

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/baf8cc9ce243ee98384337d970dd0116708e3a63

Designed website template on wix.com

- https://editor.wix.com/html/editor/web/renderer/edit/0036680a-768b-4553-9431-1671d5563dc3?metaSiteId=59087a40-3c6e-4425-9c40-be1b6d00afcc

#### Erick
Designed the website logo that would be used as an image that represents our brand and is used on the website

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/99116fcffbe77dcf14eb71459341c6e4ce71d86c
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/854727b91d64d98af766c1f3a3f596bc39b7b29e

Researched specifics about front-end such as knowledge of html and css for coding the website

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/eda85d8593acf9c6a4ec8b32222b74687a499438

Reasearched and setup my front-end environment using VistualStudioCode so I could start creating html and css files for the website

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/26ced6ed2df7e07456090286ef4a0775c44facf5

Developed the front-end of the about us page using html and css and linked it to the home page

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/f97e76d8da229cb19bf0c1041ca63ba0ce91a1df
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/bf37e14077ccda8d440e3e8681af934a6f6fedb7
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/56e1a04cf5bafde902aff6dc7046ee6eb7c4683a

Developed the front-end of the profile page using html and css and linked it to the home page

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/436f9e86cbc7bb8c74daaeb9a055bf47c59601f0
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/ef6649caa4c6fff907301b9b23d6130a766a00aa

#### Aaron



## Sprint 2:
#### Anthony


Created Express Server and Files 

 - https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/a44718c61149845debe15122d6ee9ced9434b1cd 

Wrote Code to handle AWS database connection

 - https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/7b2d226ba841805afbc206210b7862ef535a5d7e

Created Backend Routing allowing for easily readable and debuggable code 

 - https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/b13f0bffc602373fd56fff6b931ad4d6ee8c8389

Wrote code to write to user database once a user signs in 

 - https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/17a893669ab0406be6b70331d44bf40495dc9dc7

Helped debug and fix code preventing user login and sign up

 - https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/74e89aeebfb221a6bcbf34e088670126ed6e4555

Wrote Code to reroute from sign up to login to profile page

 - https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/c5b59678b442cb9fe29fabcbbd3fc9725e186e74

#### Christian
Created the react app and added the files for the pages

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/3e6a4c775d49a85b1de802dd22726761cd1fa47f

Created each page for the website and started implementing the login and signup form

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/7af983f46ac88b07c65b9181cf1c09cfc5365d72

Added login and signup to their individual forms

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/f38fc6f31461c8c2073c6c7f741df1c473e60fce

Added the login and signup in the nav bar

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/a117ddd53c4bb169b35caa93a4e9c9c11fc9f8da

Added react routing to the home page to navigate through the pages

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/1959bf49096e44c69d254de013b01c76676f3a8e

#### Erick
Researched ReactJS/JavaScript and how to setup the environment so that we could use it to connect the frontend and backend with code

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/a330ae4ebbf270a9553687cf042dd32d922d7371

Wrote code to stylize the registration and login page of the website and create responsiveness

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/6a41364d5c9bdd263cc5e9e757d01a4582efb273
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/02154d0b74e1ed4c0dfb643ba284a054c38883e5

Converted the HTML of the Home, About Us, and Profile page into React JS code and added routing to the Profile and About us page using JavaScript

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/f8b27d6a269d0973b4b556c322cf7b7e6a6a1a8c
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/930fe920c4fed17edfc166df898e57fa9d26e039
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/fca1516a0e6aa09e03f60815a2e1d34dfe486e5d
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/25983217148ebbe66e1c65d730ec87e5f17353f7 (pull request)
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/425445117a8f539dfd1e19958f98332d1d702486

#### Aaron

## Sprint 3:
#### Anthony
Write code to encrypt passwords
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/pull-requests/6/wrote-code-to-encrypt-passwords

Write Database Code to Query Reviews based on Restaurant selected
- https://bitbucket.org/cs3398-s22-ganymede/%7B6eed2e22-02b7-48b3-aebc-b7388cbf6f2d%7D/pull-requests/10

Write Code to store reviews in Database
- https://bitbucket.org/cs3398-s22-ganymede/%7B6eed2e22-02b7-48b3-aebc-b7388cbf6f2d%7D/pull-requests/13

Debug Reviews Page
 - https://bitbucket.org/cs3398-s22-ganymede/%7B6eed2e22-02b7-48b3-aebc-b7388cbf6f2d%7D/pull-requests/11
 
Create API for fetching Restaurant for drop down menu
- https://bitbucket.org/cs3398-s22-ganymede/%7B6eed2e22-02b7-48b3-aebc-b7388cbf6f2d%7D/pull-requests/16]

Wrote frontend code for receiving restaurant data 
- https://bitbucket.org/cs3398-s22-ganymede/%7B6eed2e22-02b7-48b3-aebc-b7388cbf6f2d%7D/pull-requests/17

Create API to fetch reviews 
- https://bitbucket.org/cs3398-s22-ganymede/%7B6eed2e22-02b7-48b3-aebc-b7388cbf6f2d%7D/pull-requests/19



#### Christian
added dropdown to myschool page
- https://cs3398s22ganymede.atlassian.net/browse/RMDP-61
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/2cb3b1e068cd8c16d809a3204289ed0cd25227ab

added code to render drop down menu
- https://cs3398s22ganymede.atlassian.net/browse/RMDP-65
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/57ab48ac01c16b2abfdf4e46174570e8ba9bc33a

Add a reviews page where user click on a restaurant and see the reviews
- https://cs3398s22ganymede.atlassian.net/browse/RMDP-66
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/c00f02fadbedfedf14474171c75fb434ad8fcb5f


#### Erick
documented user authorization research - https://cs3398s22ganymede.atlassian.net/browse/RMDP-71?atlOrigin=eyJpIjoiZGYzNmIxYjZjYzQxNDNlOWIyNWZhOWFhMzRmMjYyNDciLCJwIjoiaiJ9

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/1091e3bad983145629b18ec61c7dc856f24cb24c

added js functionality to review page by allowing user input - https://cs3398s22ganymede.atlassian.net/browse/RMDP-84?atlOrigin=eyJpIjoiMjk3ZmUyOWI4MzMyNGQ2NDk0N2RlNTEyN2M1YmMzZGEiLCJwIjoiaiJ9

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/c6c289864d8919a05b680dde9924b8d8cd2812f8

reformated all css files - https://cs3398s22ganymede.atlassian.net/browse/RMDP-68?atlOrigin=eyJpIjoiYWNhMDU0Y2RlM2VhNDQyNDkzMjkzMzE5MzUzNjdiZmQiLCJwIjoiaiJ9

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/28886c646ed4540612cd28d0f62f3d0256480775
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/8ef32ecc148aa50088318ac8e9aab675a5dc7a6a
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/cd6124c2dd4393ee1868fe429c2c42b123a29c4b
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/82927179ed0bf67b8a774f8e4ac5cd60c6f01090
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/71ffb5cefc104881eb70456c5004dc2d832f0383

add star rating system and format the review page - https://cs3398s22ganymede.atlassian.net/browse/RMDP-74?atlOrigin=eyJpIjoiZmRhYzhkYmEwNjVkNGI2NWE0OWY4NjQyOGQ1YTQ2ZmIiLCJwIjoiaiJ9

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/64b341de72399868b2848d0b5ee3e8f5f3202329
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/52cf0f1113cc79a199955872b2fef828aaa3d137
- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/79cdda3c3c08cb372ed3700f8bb8cd4e7c049464

fixed styling on drodown menu

- https://bitbucket.org/cs3398-s22-ganymede/ratemydiningrepository/commits/7c3e0b54da311b39e3af0d236dfccd061aa29fe8

#### Aaron


## Room for Improvement

These are the things our group as well as individuals can improve on
Room for improvement:

- Time management(start working earlier into the sprint)
- Better communication and meeting up regularly to discuss what needs to be done
- Commiunicate what each of us should be working at on simultaneously

### Next Steps
#### Sprint 2:

Current Status

- We currently have a front-end that has a home, about us, profile, and login page. We also have a backend that
  we would like to connect with the front-end to create a responsive and reactive working website that actually 
  allows functional log-in.

Anthony

- Connect AWS(Cloud) DB to server using Java and Spring, Python and Django, or Node.js
- Connect Server and DB to Front End

Christian

- create a way to create an account using html and JavaScript
- Connect the frontend to the backend
- I will implement react.js framework for the rendering of the frontend webpages

Erick

- learn JavaScript and React.JS
- figure out how to connect the frontend and backend 
- once the frontend and backend are connected get the login page to actually function properly using JS.
- get the profile page to display unique profile attributes to whoever is logged in.\

Aaron

#### Sprint 3:

Current Status

- The next step of the project is to finish the review page and only allow signed in users to access it.
- Verify user authentication and encrypt passwords

Anthony

 - Add ability to create sessions that allow users to login and view custom profile pages 

 - Write code to verify student school emails and input information to DB 

 - Add resturant and review info to Database

 - write code to encrypt and decrypt passwords 

 - write input validation for signup and login features(# of chars for password, is user email actually and email etc.)

Christian

- add routing to the reviews page and redirect once review is posted
- help with backend of reviews page
- add drop down menu to reviews page to show list of all restaurants

Erick

- implement a verification email to user's email to achieve verified student status
- add all the restaurants on campus to database of website
- get profile page to show the actual info of the person logged in

Aaron

#### Sprint 4:
Current Status

- For sprint 4, we would like to implement the star rating system.
- Create user sessions and add the profile functionality

Anthony
- Creatre sessions and cokies to authenticate logins to allow for user's to login and logout 
- Add ability fo rusers to edit and delete their revieews 
- Implment features to scan for vulgar/vague and useless reviews

Christian

- Continue styling and adding key bottoms to navigate windows
- Displaying the star rating for each review on the reviews page
- Help with user profile pulling data and putting it in the profile page

Erick
- add email validation to login page
- implement sending email verifcation to the email that is entered
- implement ability for user to upload profile picture

Aaron

## Acknowledgements

- This project was inspired by Rate My Professor. 



