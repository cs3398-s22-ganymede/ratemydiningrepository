
import React from 'react';
import './App.css';
import Login from './pages/login';
import Home from './pages/home'
import Signup from './pages/Signup';
import AboutUs from './pages/AboutUs';
import Error from './pages/Error';
import Review from './pages/Review';
import MySchool from './pages/MySchool';
import {BrowserRouter as Router, Routes , Route} from 'react-router-dom';

function App(){
  return (
    <Router> 
      <Routes> 
        <Route path ="/" element ={<Home  />}/>
        <Route path ="/aboutus" element ={<AboutUs  />}/>
        <Route path ="/MySchool" element ={<MySchool />}/>
        <Route path ="/login" element ={<Login />}/>
        <Route path ="/signup" element ={<Signup />}/>
        <Route path ="/Review" element ={<Review  />}/>
        <Route path ="*" element ={<Error />}/>
      </Routes>
    </Router>
  );
}

export default App;

