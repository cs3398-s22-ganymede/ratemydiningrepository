import React, { useState } from "react";
import { uuid } from "uuidv4"; 

export const Form = () => {
    const [form, setForm] = useState({ restaurant: "", review: "", id: null })
    
    const handleChange = e => {
        const { name, value } = e.target
        setForm({...form, [name]: value })
    }
    
    return (
        <form>
            <h2>Leave a Review</h2>
            <label htmlFor="restaurant">Restaurant</label>
            <input
                type="text"
                placeholder="Restaurant Name"
                id="restaurant"
                name="restaurant"
                autoComplete="off"
                value={form.restaurant}
                onChange={handleChange}
            />

            <label htmlFor="review">Review</label>
            <textarea 
                value={form.review} 
                placeholder="What did you think?" 
                id="review" 
                name="review"
                onChange={handleChange}
            /> 
            <button type="submit">Submit</button>
        </form>
    );
};

export default Form;