import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import styles from "./MySchool.module.css";


 function DropDown() {

  const [restaurants, setRestaurants] = useState([]);
  const [selectedRestaurant, setSelectedRestaurant] = useState([]);
  const [selected, setSelected] = useState([]);

  var temp ;
  useEffect(function() {
    axios.get( 'http://localhost:3001/fetchrestaurants') 
    .then((res) => setRestaurants(res.data))
    .then((err) => console.log(err));
  }, []);

  useEffect(function() {
    console.log(restaurants);
  }, [restaurants]);



  const HandleChange = (e) => {
    temp = e.target.value;
    axios.post('http://localhost:3001/fetchreviews',  {
        dining_id: temp
      }).then((res) => setSelectedRestaurant(res.data)).then((err) => console.log(err));
  };

  useEffect(function() {
    console.log(selectedRestaurant);
  }, [selectedRestaurant]);
  
  return (
    <div>
      <div class={styles.back}>
      <div class={styles.navbar}>
      <nav>
              <ul>
                <Link to= '/'>
                  <li>Home</li>
                </Link>

                <Link to= '/AboutUs'>
                  <li>About Us</li>
                </Link>
                
                <Link to= '/Review'>
                  <li>Review</li>
                </Link>
                
                <Link to= '/MySchool'>
                  <li>Texas State Reviews</li>
                </Link>

              </ul>
            </nav>
        </div> 
       <h2 style={{color:"#501214", fontSize: '30px',}}> Please select a restaurant to read  reviews</h2>
       <select className = "form-control col-md-3"  onChange={HandleChange}>
      < option value = {"0"}> -- Selelect a Restaurant -- </option>
      {restaurants.map((restaurant) => (
        <option key = {restaurant.id} value = {restaurant.restaurant_id}> {restaurant.name} </option>
      ))
    }
    </select>
    <br />
    <br />
    <>
                {selectedRestaurant.map(data => {
                    return(
                        <div class={styles.form}>
                          <div class={`${styles.inputcontainer} ${styles.ic1}`}>
                          <p style={{color:"white", fontSize: '15px', margin:'0px'}}>
                          </p>
                        <div className="review">
                          <h3 style={{color:"#501214"}}>{data.restaurant_name}</h3>
                            <p style={{color:"white"}}>{data.comment}</p>
                        </div>
                        </div>
                        </div>
                    )
                }
                )}
    </>
    </div>
    </div>
  );
}
    
export default DropDown;
