import React, { useState } from 'react';
import Axios from 'axios';
import { useNavigate } from "react-router-dom";
import styles from "./Login.module.css";

function Login() {

    const [emailLog, setEmailLog] = useState('');
    const [passwordLog, setPasswordLog] = useState('');

    const [loginStatus, setLoginStatus] = useState("");
    let navigate = useNavigate();


    const login = () => {
        Axios.post('http://localhost:3001/login', {
          email: emailLog,
          password: passwordLog,
        }).then((response) => {
          if(response.data.message) {
            setLoginStatus(response.data.message)}
          else {
            setLoginStatus('User logged in succesfully!');
            navigate('/profile');
          }
          console.log(response.data);
        });

        
    };

  return (
  <div class = {styles.logbody}>
   <div class={styles.logform}>
      <div class={styles.title}>Please sign in</div>
      <div class={styles.subtitle}>Enter your username and password</div>
      <div class={`${styles.inputcontainer} ${styles.ic1}`}>
        <input id="email" class={styles.input} type="text" placeholder=" "
         onChange={(e) => {
          setEmailLog(e.target.value);
        }}
        />
        <div class={styles.cut}></div>
        <label for="email" class={styles.placeholder}>Email</label></div>
      <div class={`${styles.inputcontainer} ${styles.ic2}`}>
        <input id="password" class={styles.input} type="password" placeholder=" " 
        onChange={(e) => {
          setPasswordLog(e.target.value);
        }}
        />
      <div class={styles.cut}></div>
      <label for="password" class={styles.placeholder}>Password</label>
     </div>


      <button type="text" onClick={login} class={styles.submit} >Login</button>
      <h4>{loginStatus}</h4>
    </div>
    </div>
    )
}


export default Login;