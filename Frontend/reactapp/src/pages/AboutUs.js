import react from 'react'
import styles from './AboutUs.module.css';
import { Link } from 'react-router-dom';

function aboutus(){
    return(
        <div>
        <meta charSet="UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="/Frontend/css_files/aboutus.css" />
        <link rel="icon" href="/Frontend/images/ratemydining logo2.png" />
        <title>About Us</title>
        <div className={styles.acontainer}>
          <div className={styles.navbar}>
            <img src={require("./images/ratemydining logo.png")} alt="logo" width={175} height={175} />
            <link rel="logo" href="/Frontend/html_files/index.html" />
            <nav>
              <ul>
                <Link to= '/'>
                  <li>Home</li>
                </Link>

                <Link to= '/AboutUs'>
                  <li>About Us</li>
                </Link>
                
                <Link to= '/Review'>
                  <li>Review</li>
                </Link>
                
                <Link to= '/MySchool'>
                  <li>Texas State Reviews</li>
                </Link>

              </ul>
            </nav>
          </div>
        </div>
        <div className={styles.textop}>
          <h1>About Us</h1>
          <div className = {styles.atext}>
            <p style={{color: "501214"}}>
                RateMyDining is a website for verified students to post reviews, view reviews, and communicate with their
                campus peers about their dining preferences. By allowing students to post verified reviews about campus restaurants,
                 their campus peers will be ensured to have quality reviews about the places they choose to dine at on campus. RateMyDining 
                is more than a review website, we hope for students to communicate with each other and make connections based off dining 
                preferences. 
            </p>
          </div>
        </div>
      </div>
    
    )
}

export default aboutus