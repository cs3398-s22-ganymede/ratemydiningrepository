import React, { useState } from 'react';
import Axios from 'axios';
import { useNavigate } from "react-router-dom";
import styles from './Signup.module.css'

function Registration() {
    let navigate = useNavigate();
    const [firstSignup, setFirstSignUp] = useState('');
    const [lastSignup, setLastSignUp] = useState('');
    const [userSignup, setUserSignUp] = useState('');
    const [emailSignup, setEmailSignUp] = useState('');
    const [passwordSignup, setPasswordSignUp] = useState('');
    
    const signUp = () => {
        Axios.post('http://localhost:3001/signup', {
          email: emailSignup,
          password: passwordSignup,
          userName: userSignup,
          firstName: firstSignup,
          lastName: lastSignup,
        }).then((response) => {
          console.log(response);
        });
        navigate('/login');
        


      };

   return (
  <div class = {styles.subody}>
    <div class={styles.form}>
      <div class={styles.title}>Welcome to Rate My Dining!</div>
      <div class={styles.subtitle}>Let's create your account!</div>
      <div class={`${styles.inputcontainer} ${styles.ic1}`}>
        <input id="firstname" class={styles.input} type="text" placeholder=" "
         onChange={(e) => {
          setFirstSignUp(e.target.value);
        }}
        />
        <div class={styles.cut}></div>
        <label for="firstname" class={styles.placeholder}>First name</label>
      </div>
      <div class={`${styles.inputcontainer} ${styles.ic4}`}>
        <input id="lastname" class={styles.input} type="text" placeholder=" " 
        onChange={(e) => {
          setLastSignUp(e.target.value);
        }}
        />
        <div class={styles.cut}></div>
        <label for="lastname" class={styles.placeholder}>Last Name</label>
      </div>
      <div class={`${styles.inputcontainer} ${styles.ic2}`}>
        <input id="username" class={styles.input} type="text" placeholder=" " 
         onChange={(e) => {
          setUserSignUp(e.target.value);
        }}
        />
        <div class={styles.cut}></div>
        <label for="username" class={styles.placeholder}>Username</label>
      </div>
    <div class={`${styles.inputcontainer} ${styles.ic2}`}>
        <input id="email" class={styles.input} type="text" placeholder=" "
         onChange={(e) => {
          setEmailSignUp(e.target.value);
        }}
        />
        <div class={`${styles.cut} ${styles.cutshort}`}></div>
        <label for="email" class={styles.placeholder}>Email</label>
    </div>
    <div class={`${styles.inputcontainer} ${styles.ic3}`}>
        <input id="password" class={styles.input} type="password" placeholder=" " 
         onChange={(e) => {
          setPasswordSignUp(e.target.value);
        }}
        />
        <div class={`${styles.cut} ${styles.cut}`}></div>
        <label for="password" class={styles.placeholder}>Password</label>

    </div>
      <button type="text" onClick={signUp} class={styles.submit}>Sign Up</button>
    </div>
  </div>
   )
}


export default Registration;