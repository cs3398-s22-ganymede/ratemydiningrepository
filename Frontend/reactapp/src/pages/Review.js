import React from 'react';
import Form from './components/Form';
import styles from './Review.module.css';
import Axios from 'axios';
import { useState } from 'react';
import { useNavigate } from "react-router-dom";
import { Link } from 'react-router-dom';
import StarRating from './StarRating';
import { useEffect } from "react";


function Review() {
    let navigate = useNavigate();
    const [reviewText, setReviewText] = useState('');
    const [restaurantID, setRestaurantID] = useState('');
    const [restaurants, setRestaurants] = useState([]);
    const [qrating, setQRating] = useState(null);
    const [mrating, setMRating] = useState(null);
    const [prating, setPRating] = useState(null);
    const [trating, setTRating] = useState(null);
    const [arating, setARating] = useState(null);
   

    useEffect(function() {
      Axios.get( 'http://localhost:3001/fetchrestaurants') 
      .then((res) => setRestaurants(res.data))
      .then((err) => console.log(err));
    }, []);
    const Submissions = () => {
      
            Axios.post('http://localhost:3001/reviews', {
            reviewtxt: reviewText,
            rest_id: restaurantID,
            
            /*
            quality_rating : StarRating.rating,
            menu_rating : StarRating.rating,
            price_rating : StarRating.rating,
            traffic_rating :  StarRating.rating,
            atmosphere_rating :  StarRating.rating,*/


        }).then((response) => {
            console.log(response);
         });
    };

  

    return (
    <div class = {styles.rcontainer}>
      <div className={styles.navbar}>
        <img src={require("./images/ratemydining logo.png")} alt="logo" width={175} height={175} />
        <link rel="logo" href="/Frontend/html_files/index.html" />
        <nav>
          <ul>
            <Link to= '/'>
              <li>Home</li>
            </Link>

            <Link to= '/AboutUs'>
              <li>About Us</li>
            </Link>

            <Link to= '/Review'>
              <li>Review</li>
            </Link>

            <Link to= '/MySchool'>
              <li>Texas State Reviews</li>
            </Link>

            
          </ul>
        </nav>
      </div>
        <div class={styles.form}>
          <div class={styles.title}>Welcome to Rate My Dining!</div>
          <div class={styles.subtitle}>Leave a review about your dining experience!</div>
          <div class={`${styles.inputcontainer} ${styles.ic1}`}>
          <p style={{color:"white", fontSize: '15px', margin:'0px'}}>Select a Restaurant
          </p>
       <select  className = "form-control col-md-3"  onChange= { (e) => setRestaurantID(e.target.value) }>
      < option style={{color:"black"}} value = {"0"}> -- Select a Restaurant -- </option>
      {restaurants.map((restaurant) => (
        <option style={{color:"black"}} key = {restaurant.id} value = {restaurant.restaurant_id}> {restaurant.name} </option>
      ))
    }
    </select>
            <div class={styles.cut}></div>
            <label for="restaurantname" class={styles.placeholder}></label>
          </div>
          <div class={`${styles.inputcontainer2} ${styles.ic4}`}>
            <textarea id="reviewtext" class={styles.input} type="text" placeholder=" " 
            onChange={(e) => {
              setReviewText(e.target.value);
            }}
            />
            <div class={styles.cutbig}></div>
            <label for="reviewtext" class={styles.placeholder}>Leave your written review here</label>
            

        </div>
          <button type="text" onClick={Submissions} class={styles.submit}>Submit Review</button>
        </div>
    </div>         
    )
}



export default Review;

