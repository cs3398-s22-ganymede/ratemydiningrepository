import React from 'react';

function Nav() {
    return (
        <nav>
            <h3>Logo</h3>
            <ul>
                <li>Home</li>
                <li>Profile</li>
                <li>Login</li>
                <li>Registration</li>
            </ul>
        </nav>
    )
}


export default Nav;