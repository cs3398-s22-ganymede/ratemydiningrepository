import React from 'react';
import './Profile.css'
import { Link } from 'react-router-dom';

function profile() {
    return (
        <div>
        <meta charSet="UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="/Frontend/css_files/profile.css" />
        <link rel="icon" href="/Frontend/images/ratemydining logo2.png" />
        <title>Profile</title>
        <div className="student-profile">
          <div className="pcontainer">
            <div className="navbar">
              <img src={require("./images/ratemydining logo.png")} alt="logo" width={175} height={175} />
              <link rel="logo" href="/Frontend/html_files/index.html" />
              <nav>
                <ul>
                <Link to= '/'>
                  <li>Home</li>
                </Link>

                <Link to= '/AboutUs'>
                  <li>About Us</li>
                </Link>


                </ul>
              </nav>
            </div>
            <div className="adjust">
              <div className="prow">
                <div className="col-lg-4">
                  <div className="card shadow-sm">
                    <div className="card-header bg-transparent text-center">
                      <img className="profile_img" src={require("./images/userprofileimg.jpg")} alt="student dp" />
                      <h3>Student Name</h3>
                    </div>
                    <div className="card-body">
                      <p className="mb-0"><strong className="pr-1">Verification Status:</strong>Verified</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-8">
                  <div className="card shadow-sm">
                    <div className="card-header bg-transparent border-0">
                      <h3 className="mb-0"><i className="far fa-clone pr-1" />Your Information</h3>
                    </div>
                    <div className="card-body pt-0">
                      <table className="table table-bordered">
                        <tbody><tr>
                            <th width="30%">Email</th>
                            <td width="2%">:</td>
                            <td>studentuseremail@txstate.edu</td>
                          </tr>
                          <tr>
                            <th width="30%">Reviews Posted	</th>
                            <td width="2%">:</td>
                            <td>36</td>
                          </tr>
                          <tr>
                            <th width="30%">Reviews Viewed</th>
                            <td width="2%">:</td>
                            <td>50</td>
                          </tr>
                          <tr>
                            <th width="30%">Number of Friends</th>
                            <td width="2%">:</td>
                            <td>12</td>
                          </tr>
                          <tr>
                            <th width="30%">Number of Likes</th>
                            <td width="2%">:</td>
                            <td>376</td>
                          </tr>
                        </tbody></table>
                    </div>
                  </div>
                  <div style={{height: '26px'}} />
                  <div className="card shadow-sm">
                    <div className="card-header bg-transparent border-0">
                      <h3 className="mb-0"><i className="far fa-clone pr-1" />Bio</h3>
                    </div>
                    <div className="card-body pt-0">
                      <p>Enter your bio here describing yourself or your dining preferences so that other student users can see.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}


export default profile;