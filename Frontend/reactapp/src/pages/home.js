import React from 'react'
import styles from  './home.module.css';
import { Link } from 'react-router-dom';
//comment
function home() {
  return (
    <div>
    <title>RateMyDining - Rate campus dining</title>
    <link rel="stylesheet" href="/pages/Home.css" />
    <link rel="icon" href="/src/ratemydining logo2.png" />
    <div className={styles.hcontainer}>
      <div className={styles.navbar}>
        <img src={require("./images/ratemydining logo.png")} alt="logo" width={175} height={175} />
        <link rel="logo" href="/Frontend/html_files/index.html" />
        <nav>
          <ul>
            <Link to= '/'>
              <li>Home</li>
            </Link>

            <Link to= '/AboutUs'>
              <li>About Us</li>
            </Link>

            <Link to= '/Review'>
              <li>Review</li>
            </Link>

            <Link to= '/MySchool'>
              <li>Texas State Reviews</li>
            </Link>
            
          </ul>
        </nav>
      </div>
      <div className={styles.row}>
        <div className={styles.hcol}>
          <h1>Welcome to RateMyDining!</h1>
          <p2> 
            The perfect place to post and read reviews for your college campus restaurants.
          </p2>
        </div>
        <div className={styles.hcol}>
        </div>
      </div>
    </div>
  </div>
  )
}

export default home