-- @block SELECT DATABASE();
-- @block 
SHOW DATABASES;
-- @block 
SELECT DATABASE();
-- @block 
CREATE TABLE Users(
  user_id INT PRIMARY KEY AUTO_INCREMENT, 
  first_name varchar(255) NOT NULL, 
  last_name varchar(255) NOT NULL,
  email varchar(255) NOT NULL UNIQUE,
  verifeid_student boolean NOT NULL
);

-- @block
ALTER TABLE Users
ADD password varchar(255) NOT NULL;

-- @block
ALTER TABLE Users
ADD username VARCHAR(255) NOT NULL;

-- @block 
ALTER TABLE USERS 
CREATE CONSTRAINT username UNIQUE 

-- @block 
CREATE TABLE Schools (
  school_id INT PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  city varchar(255) NOT NULL,
  state varchar(255) NOT NULL,
  zip int NOT NULL
);

-- @block
CREATE TABLE Restaurants (
  restaurant_id INT PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  avg_rating int NOT NULL,
  parent_school_id int NOT NULL,
  FOREIGN KEY (parent_school_id) REFERENCES Schools(school_id)
);

-- @block
CREATE TABLE Reviews (
  parent_restaurant_id INT PRIMARY KEY,
  parent_school_id INT NOT NULL,
  reviewer_id int NOT NULL,
  overall_rating int NOT NULL,
  traffic_rating int NOT NULL,
  quality_rating int NOT NULL,
  menu_rating int NOT NULL,
  price_rating int NOT NULL,
  atmosphere_rating int NOT NULL,
  comment text NOT NULL,
  FOREIGN KEY (reviewer_id) REFERENCES Users(user_id),
  FOREIGN KEY (parent_school_id) REFERENCES Schools (school_id),
  FOREIGN KEY (parent_restaurant_id) REFERENCES Restaurants (restaurant_id)
);

