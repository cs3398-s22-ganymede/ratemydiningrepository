const express = require('express');
const mysql = require('mysql');
const router = express.Router();
const app = express();
const cors = require('cors');
const bcrypt = require('bcrypt');
const saltRounds = 10;

app.use(express.json());
app.use(cors());

const db = mysql.createConnection({
  host: "rmd-db.cam0y77ipci4.us-east-1.rds.amazonaws.com",
  port: "3306",
  user: "admin",
  password: "admin1234$", //figure out how to protect password 
  database: "rmddb",
});

db.connect((err) => {
});


// code for signup/register
router.post('/', (req, res) => {

  const firstname = req.body.firstName;
  const lastname = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const username = req.body.userName
  bcrypt.hash(password, saltRounds, (err, hash) => {

    if (err) {
      console.log(err);
    }
    db.query("INSERT INTO Users (first_name, last_name, email, password, username) VALUES (?,?,?,?,?)", [firstname, lastname, email, hash, username], (err, result) => {
      
      if (err) {
        console.log(err); 
      }

      console.log("New user added...");
    })
  })
});


module.exports = router;