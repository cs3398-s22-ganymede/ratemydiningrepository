const express = require('express');
const mysql = require('mysql');
const app = express();
const cors = require('cors');



const signupRouter = require('./routes/signup-server');
const loginRouter = require('./routes/login-server');
const reviewsRouter = require('./routes/reviews-server');
const fetchRestaurantsRouter = require('./routes/fetch-restaurants');
const fetchReviewsRouter = require('./routes/fetch-reviews');

app.use(express.json());
app.use(cors());
app.use('/signup', signupRouter);
app.use('/login', loginRouter);
app.use('/reviews', reviewsRouter);
app.use('/fetchrestaurants', fetchRestaurantsRouter);
app.use('/fetchreviews', fetchReviewsRouter);

const db = mysql.createConnection({
  host: "rmd-db.cam0y77ipci4.us-east-1.rds.amazonaws.com",
  port: "3306",
  user: "admin",
  password: "admin1234$", //figure out how to protect password 
  database: "rmddb",
});

db.connect((err) => {
  if (err) {
    console.log(err.message);
  }
  console.log("MySQL Connected...");
});

app.listen(3001, () =>{
  console.log("Listening on port 3001");
});